{
  "version": "2",
  "templates": [
    {
      "type": 1,
      "title": "Watchtower",
      "description": "A container-based solution for automating Docker container base image updates.",
      "note": "Update containers that have a com.centurylinklabs.watchtower.enable label set to true.",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/watchtower.png",
      "image": "containrrr/watchtower:latest",
      "env": [
        {
          "name": "TZ",
          "label": "Timezone",
          "default": "Europe/Zurich"
        },
        {
          "name": "WATCHTOWER_LABEL_ENABLE",
          "default": "true",
          "preset": true
        }
      ],
      "volumes": [
        {
          "container": "/var/run/docker.sock",
          "bind": "/var/run/docker.sock"
        }
      ]
    },
    {
      "type": 2,
      "title": "EMQX 4.3.5",
      "description": "EMQ X MQTT broker is a fully open source, highly scalable, highly available distributed MQTT messaging broker for IoT, M2M and Mobile applications that can handle tens of millions of concurrent clients.",
      "note": "Webinterface will be reachable on http://your-server-ip:dashboard-port/. <b>First username and password are: admin public</b><br><br>Add 'emqx_auth_username' from the plugins menu on the dashboard. Manage mqtt users with this plugin because anonymous auth is disabled.",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/emqx.svg",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/emqx/docker-stack.yml"
      },
      "env": [
        {
          "name": "EX_MQTT_PORT",
          "label": "Mqtt port",
          "default": "1883"
        },
        {
          "name": "EX_DASHBOARD_PORT",
          "label": "Dashboard port",
          "default": "18083"
        }
      ]
    },
    {
      "type": 2,
      "title": "Plex Server",
      "description": "With our easy-to-install Plex Media Server software and your Plex apps, available on all your favorite phones, tablets, streaming devices, gaming consoles, and smart TVs, you can stream your video, music, and photo collections any time, anywhere, to any device.",
      "note": "Webinterface will be reachable on http://your-server-ip:32400/web",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/plex.jpg",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/plex/docker-stack.yml"
      },
      "env": [
        {
          "name": "PS_PLEX_UID",
          "label": "User Id"
        },
        {
          "name": "PS_PLEX_GID",
          "label": "Group Id"
        },
        {
          "name": "PS_CLAIM_TOKEN",
          "label": "Server Claim token",
          "description": "You can obtain a claim token to login your server to your plex account by visiting https://www.plex.tv/claim."
        },
        {
          "name": "PS_NFS_SERVER_IP",
          "label": "NFS Server"
        },
        {
          "name": "PS_NFS_SERVER_CONFIG_PATH",
          "label": "Plex config path",
          "default": "/plex/config"
        },
        {
          "name": "PS_NFS_SERVER_TV_PATH",
          "label": "Plex data path",
          "default": "/plex/tv"
        },
        {
          "name": "PS_NFS_SERVER_MOVIES_PATH",
          "label": "Plex transcode path",
          "default": "/plex/movies"
        }
      ]
    },
    {
      "type": 2,
      "title": "Storj",
      "description": "Storj is an S3-compatible platform and suite of decentralized applications that allows you to store data in a secure and decentralized manner.",
      "note": "You need to forward port 28967. There is a webinterface on port 14002.",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/storj.png",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/storj/docker-stack.yml"
      },
      "env": [
        {
          "name": "ST_USER",
          "label": "User Id"
        },
        {
          "name": "ST_GROUP",
          "label": "Group Id"
        },
        {
          "name": "ST_WALLET",
          "label": "ETH Wallet"
        },
        {
          "name": "ST_EMAIL",
          "label": "E-Mail"
        },
        {
          "name": "ST_ADDRESS",
          "label": "Public ip or dns and port (28967)",
          "description": "e.g. storj.mydomain.com:28967"
        },
        {
          "name": "ST_STORAGE",
          "label": "Storage amount",
          "description": "e.g. 9TB"
        },
        {
          "name": "ST_NFS_SERVER_IP",
          "label": "NFS Server"
        },
        {
          "name": "ST_NFS_SERVER_DATA_PATH",
          "label": "Storj data path"
        },
        {
          "name": "ST_NFS_SERVER_IDENTITY_PATH",
          "label": "Storj identity path",
          "description": "Main path incl. storagenode"
        }
      ]
    },
    {
      "type": 2,
      "title": "Traefik 2.5",
      "description": "Traefik is an open-source Edge Router that makes publishing your services a fun and easy experience",
      "note": "<p>Traefik will serve on port 80 and 443. All requests on port 80 will be redirected to 443. Traefik will only be deployed on Nodes with the following tag.</p><div><ul><li>traefik-public.traefik-public-certificates=true</li></ul></div><p>Before you deploy traefik create a network with the name networks:</p><ul><li>traefik-network (encrypted=true)</li></ul><p>You also need to provide the following secrets:</p><ul><li>godaddy_key</li><li>godaddy_secret</li></ul><p>You can use this labels on <strong>services (deploy: tag)</strong> which take use of traefik</p><div><ul><li>traefik.enable=true</li><li>traefik.http.routers.whoami.rule=Host(`whoami.example.com`)</li><li>traefik.http.routers.whoami.entrypoints=websecure</li><li>traefik.http.routers.whoami.tls.certresolver=gdresolver</li><li>traefik.http.services.whoami.loadbalancer.server.port=80 # Port der Applikation</li></ul><p>&nbsp;</p><p><strong>Sample service configuration for traefik provided as WhoAmI App Template!</strong></p></div>",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/traefik.logo.png",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/traefik/docker-stack.yml"
      },
      "env": [
        {
          "name": "TRAEFIK_LETSENCRYPT_EMAIL",
          "label": "E-Mail (Lets Encrypt)",
          "description": "Valid E-Mail for Lets Encrypt certificates"
        }
      ]
    },
    {
      "type": 2,
      "title": "WhoAmI",
      "description": "WhoAmI is a sample/test service for traefik proxy",
      "note": "<div><div>- Use this to test lets encrypt integration or use it as a template for other stacks.</div><div>&nbsp;</div><div>- After successfull deployment the service is avalable under the choosen domain.</div></div>",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/traefik.logo.png",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/whoami/docker-stack.yml"
      }
    },
    {
      "type": 2,
      "title": "Nextcloud",
      "description": "A safe home for all your data. Access & share your files, calendars, contacts, mail & more from any device, on your terms.",
      "note": "Nextcloud will connect to your traefik network so make shure traefik is running.<br><br>Add ''trusted_proxies' => array (0 => 'traefikRouter'), 'overwriteprotocol' => 'https',' to config/config.php<br><br>You need to provide the following secrets before you are able to start this stack.<br><ul><li>nextcloud_postgresdb_db</li><li>nextcloud_postgresdb_user</li><li>nextcloud_postgresdb_pwd</li><li>nextcloud_admin_user</li><li>nextcloud_admin_pwd</li></ul>",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/nextcloud.svg",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/nextcloud/docker-stack.yml"
      },
      "env": [
        {
          "name": "NC_DOMAIN_NAME",
          "label": "Domain name",
          "description": "Your nextcloud domain name."
        },
        {
          "name": "NC_NFS_SERVER_IP",
          "label": "NFS Server IP"
        },
        {
          "name": "NC_NFS_SERVER_DB_PATH",
          "label": "NFS DB path",
          "description": "Database directory"
        },
        {
          "name": "NC_NFS_SERVER_NEXTCLOUD_PATH",
          "label": "NFS www root path",
          "description": "Nextcloud www root directory"
        },
        {
          "name": "NC_NFS_SERVER_APPS_PATH",
          "label": "NFS Apps path",
          "description": "Nextcloud apps directory"
        },
        {
          "name": "NC_NFS_SERVER_CONFIG_PATH",
          "label": "NFS Config path",
          "description": "Nextcloud config directory"
        },
        {
          "name": "NC_NFS_SERVER_DATA_PATH",
          "label": "NFS Data path",
          "description": "Nextcloud data directory"
        }
      ]
    },
    {
      "type": 2,
      "title": "Unifi Controller",
      "description": "The Unifi-controller Controller software is a powerful, enterprise wireless software engine ideal for high-density client deployments requiring low latency and high uptime performance.",
      "note": "Unifi Controller will use the following ports: 3478/udp, 10001/udp and 8080. Please make shure that they are not in use already!",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/ubiquiti_logo.png",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/unificontroller/docker-stack.yml"
      },
      "env": [
        {
          "name": "UC_VERSION",
          "label": "Unifi Controller Version",
          "default": "Available version: https://hub.docker.com/r/linuxserver/unifi-controller/tags"
        },
        {
          "name": "UC_USER_ID",
          "label": "User ID"
        },
        {
          "name": "UC_GROUP_ID",
          "label": "Group ID"
        },
        {
          "name": "UC_MEM_LIMIT",
          "label": "Memory limit",
          "default": "1024M"
        },
        {
          "name": "UC_HTTPS_PORT",
          "label": "Webinterface port",
          "default": "8443"
        },
        {
          "name": "UC_NFS_SERVER_IP",
          "label": "NFS Server IP"
        },
        {
          "name": "UC_NFS_SERVER_CONFIG_PATH",
          "label": "NFS config path",
          "default": "/unificontroller/config",
          "description": "Unifi Controller config directory"
        }
      ]
    },
    {
      "type": 2,
      "title": "DDClient",
      "description": "Ddclient is a Perl client used to update dynamic DNS entries for accounts on Dynamic DNS Network Service Provider",
      "note": "<ol><li>Create an account at <a href='https://www.dnsomatic.com/'>DNS-O-Matic</a></li><li>Create a config file from <a href='https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/templates/stack/ddclient/ddclient.conf?inline=false'>ddclient-template.conf</a></li><li>Provide this config file as a secret with the name: <b>ddclient_conf</b></li></ol>",
      "categories": ["dns"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/ddclient-logo.png",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/ddclient/docker-stack.yml"
      },
      "env": [
        {
          "name": "DC_TZ",
          "label": "Timezone",
          "default": "Europe/Zurich"
        },
        {
          "name": "DC_UID",
          "label": "User ID",
          "default": "1005"
        },
        {
          "name": "DC_GID",
          "label": "Group ID",
          "default": "1005"
        }
      ]
    },
    {
      "type": 2,
      "title": "Blocky",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/blocky.svg",
      "description": "Blocky is a DNS proxy for the local network.",
      "note": "The template creates a functional default config 'blocky-config.yml' which you can change for your needs. Sample configurations can be found here: <a href=\"https://hub.docker.com/r/spx01/blocky\">https://hub.docker.com/r/spx01/blocky</a>",
      "categories": ["dns"],
      "platform": "linux",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/blocky/docker-stack.yml"
      },
      "env": [
        {
          "name": "BLOCKY_TZ",
          "label": "Timezone",
          "default": "Europe/Zurich"
        }
      ]
    },
    {
      "type": 2,
      "title": "OpenHab 3.1.0 / EMQX 4.3.5 / Frontail 4.9.2",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/openhab.svg",
      "description": "openHAB - a vendor and technology agnostic open source automation software for your home. This stack is also deploying the Mqtt Broker EMQX!",
      "note": "<p><strong>Traefik Proxy needs to run before this stack is deployed!</strong></p><p>The following services will be available under the given domain.</p><ul><li>openhab.[Domain]</li><li>emqx.[Domain]</li><li>frontail-oh.[Domain]</li></ul>",
      "categories": ["homeautomation"],
      "platform": "linux",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/openhab/docker-stack.yml"
      },
      "env": [
        {
          "name": "OH_TZ",
          "label": "Timezone",
          "default": "Europe/Zurich"
        },
        {
          "name": "OH_HTTP_PORT",
          "label": "HTTP Port",
          "default": "8080"
        },
        {
          "name": "OH_HTTPS_PORT",
          "label": "HTTPS Port",
          "default": "8443"
        },
        {
          "name": "OH_STACK_DOMAIN",
          "label": "Domain",
          "description": "Domain which all services will be reachable. Traefik stack must run for https certifiace integration"
        },
        {
          "name": "OH_USER_ID",
          "label": "User ID",
          "description": "Use your UID from the NAS user, otherwise you maybe wont be able to access the files outside the container.",
          "default": "9001"
        },
        {
          "name": "OH_GROUP_ID",
          "label": "Group ID",
          "description": "Use your GUID from the NAS user, otherwise you maybe wont be able to access the files outside the container.",
          "default": "9001"
        },
        {
          "name": "OH_NFS_SERVER_IP",
          "label": "NFS Server IP"
        },
        {
          "name": "OH_NFS_SERVER_ADDON_PATH",
          "description": "OpenHab addons path",
          "label": "NFS addons path",
          "default": "/openhab/addons"
        },
        {
          "name": "OH_NFS_SERVER_CONF_PATH",
          "description": "OpenHab configuration path",
          "label": "NFS config path",
          "default": "/openhab/conf"
        },
        {
          "name": "OH_NFS_SERVER_USERDATA_PATH",
          "description": "OpenHab userdata path",
          "label": "NFS userdata path",
          "default": "/openhab/userdata"
        },
        {
          "name": "FT_HTTP_PORT",
          "label": "Frontail: HTTP Port",
          "default": "9001"
        },
        {
          "name": "EX_MQTT_PORT",
          "label": "EMQX: Mqtt port",
          "default": "1883"
        },
        {
          "name": "EX_DASHBOARD_PORT",
          "label": "EMQX: Dashboard port",
          "default": "18083"
        }
      ]
    },
    {
      "type": 2,
      "title": "Diun",
      "description": "Docker Image Update Notifier is a CLI application written in Go and delivered as a single executable (and a Docker image) to receive notifications when a Docker image is updated on a Docker registry.",
      "note": "<p>To watch image updates for services add the following tags in the <strong>deploy</strong> section</p><ul><li>diun.enable=true</li><li>diun.watch_repo=true</li></ul><p>Get push notification with Pushover - https://pushover.net</p><p>Use your pushover User Key and also create a new application for the api token</p>",
      "categories": ["infrastructure"],
      "platform": "linux",
      "logo": "https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/logos/diu.png",
      "repository": {
        "url": "https://gitlab.com/infrastructure12/portainer-stacks",
        "stackfile": "templates/stack/diun/docker-stack.yml"
      },
      "env": [
        {
          "name": "DIUN_TZ",
          "label": "Timezone",
          "default": "Europe/Zurich"
        },
        {
          "name": "DIUN_CRON",
          "description": "Cron template https://crontab.guru/",
          "label": "Cron",
          "default": "0 */6 * * *"
        },
        {
          "name": "DIUN_POTOKEN",
          "label": "Pushover App Token"
        },
        {
          "name": "DIUN_PORECIPIENT",
          "label": "Pushover User Key"
        }
      ]
    }
  ]
}
