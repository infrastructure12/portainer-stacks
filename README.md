# Portainer-Stacks

Portainer Stacks for local infrastrucure. (https://www.portainer.io/)

## Install Docker Swarm

1. Download and install HypriotOS (only for Raspberry Pi) - [Instructions](https://blog.hypriot.com/post/how-to-setup-rpi-docker-swarm/)
    1. Or install any other OS with Docker - [Instructions](https://docs.docker.com/engine/install/)
1. Install Docker Swarm
`docker swarm init --advertise-addr <MANAGER-IP>` - [Instructions](https://docs.docker.com/engine/swarm/swarm-tutorial/create-swarm/)
1. Add additional nodes if needed

## Install Portainer with SSL

1. Generate SSL Certification and Key. `openssl req -nodes -new -x509 -keyout portainer.key -out portainer.crt`
2. Create Docker secrets for .crt and .key file `docker secret create <secretname> <secret file>` - [Instructions](https://documentation.portainer.io/v2.0/deploy/ssl/)
3. Start Docker service with this compose file `docker stack deploy -c portainer-agent-stack.yml portainer`

```
version: '3.2'

services:
  agent:
    image: portainer/agent:latest
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker/volumes:/var/lib/docker/volumes
    networks:
      - portainer
    deploy:
      mode: global
      placement:
        constraints: [node.platform.os == linux]

  portainer:
    image: portainer/portainer-ce:latest
    command: -H tcp://tasks.agent:9001 --tlsskipverify --ssl --sslcert /run/secrets/portainer.crt --sslkey /run/secrets/portainer.key
    ports:
      - "9000:9000"
      - "8000:8000"
    volumes:
      - portainer_data:/data
    networks:
      - portainer
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints: [node.role == manager]
    secrets:
        - portainer.crt
        - portainer.key

networks:
  portainer:
    driver: overlay
    attachable: true

volumes:
  portainer_data:

secrets:
  portainer.crt:
    external: true
  portainer.key:
    external: true
```

## Add Templates to portainer config

In the portainer webinterface/settings add the following url `https://gitlab.com/infrastructure12/portainer-stacks/-/raw/master/templates/templates.json`